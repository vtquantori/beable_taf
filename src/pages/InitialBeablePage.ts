import BasePage from 'src/pages/BasePage';


class InitialBeablePage extends BasePage {
    constructor() {
        super(`/`);
    }

    get getStartedButton(): WebdriverIO.Element {
        return $(`button=GET STARTED`);
    }
}

export default new InitialBeablePage();
