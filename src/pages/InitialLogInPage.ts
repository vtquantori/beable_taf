import BasePage from 'src/pages/BasePage';


class InitialLoginPage extends BasePage {
    constructor() {
        super(`/initial-login`);
    }

    get schoolStateInput(): WebdriverIO.Element {
        return $(`#school-state-select`);
    }

    get districtInput(): WebdriverIO.Element {
        return $(`#school-district-select`);
    }

    get letsGoButton(): WebdriverIO.Element {
        return $(`button=LET'S GO!`);
    }

    public authorize(schoolState: string, district: string): void {
        this.schoolStateInput.setValue(schoolState);
        this.districtInput.setValue(district);
        this.letsGoButton.click();
    }

    get errorMsgElement(): WebdriverIO.Element {
        return $('#flash');
    }

    getErrorMessage(): string {
        return this.errorMsgElement.getText();
    }
}

export default new InitialLoginPage();
