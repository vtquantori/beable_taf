import { assert } from 'chai';


export default class BasePage {
    constructor(protected readonly path: string) {}

    public navigateTo(): void {
        browser.url(this.path);
    }

    public assertIsNavigatedToPagePath(): void {
        assert.equal(new URL(browser.getUrl()).pathname, this.path, `Browser is not navigated to path '${this.path}'`);
    }
}

