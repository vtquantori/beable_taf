import BasePage from 'src/pages/BasePage';


class FinalLoginPage extends BasePage {
    constructor() {
        super(`/final-login`);
    }

    get usernameInput(): WebdriverIO.Element {
        return $(`#username`);
    }

    get passwordInput(): WebdriverIO.Element {
        return $(`#password`);
    }

    get submitButton(): WebdriverIO.Element {
        return $(`button=LET'S GO!`);
    }

    public authorize(username: string, password: string): void {
        this.usernameInput.setValue(username);
        this.passwordInput.setValue(password);
        this.submitButton.click();
    }

    get errorMsgElement(): WebdriverIO.Element {
        return $('#flash');
    }

    getErrorMessage(): string {
        return this.errorMsgElement.getText();
    }
}

export default new FinalLoginPage();
