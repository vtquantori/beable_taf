
## How to install
If required: install NodeJS and TypeScript 
and get node modules with 
`npm install`

## How to run
 `npm test`

## How to get report
`npm run report`
