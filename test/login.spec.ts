
import { expect } from 'chai';

import FinalLoginPage from 'src/pages/FinalLoginPage';
import InitialBeablePage from 'src/pages/InitialBeablePage';
import InitialLoginPage from 'src/pages/InitialLoginPage';


describe('Login page test', () => {

    it(`Get authorized`, () => {
        InitialBeablePage.getStartedButton.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.authorize(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.authorize(`real_username`, `real_password`);
    });

    // Example of scenarios with possible errors if wrong value

    it('Error if login with wrong password', () => {
        InitialLoginPage.navigateTo();
        InitialBeablePage.getStartedButton.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.authorize(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.authorize(`real_username`, `not_existed_password`);

        expect(FinalLoginPage.getErrorMessage()).to.include('Invalid username or password!');
    });

    it('Error if login with wrong username', () => {
        InitialLoginPage.navigateTo();
        InitialBeablePage.getStartedButton.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.authorize(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.authorize(`not_existed_username`, `real_password`);

        expect(FinalLoginPage.getErrorMessage()).to.include('Invalid username or password!');
    });

    it('Error if login with empty password', () => {
        InitialLoginPage.navigateTo();
        InitialBeablePage.getStartedButton.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.authorize(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.authorize(`real_username`, ``);

        expect(FinalLoginPage.getErrorMessage()).to.include('Password is required!');
    });

    it('Error if login with empty username', () => {
        InitialLoginPage.navigateTo();
        InitialBeablePage.getStartedButton.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.authorize(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.authorize(``, `real_password`);

        expect(FinalLoginPage.getErrorMessage()).to.include('Username is required!');
    });
});